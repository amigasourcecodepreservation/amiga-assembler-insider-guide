*	Example CH7-2.S

START move.b  X,Y copy byte X to byte Y 
      rts
X	    dc.b 10	allocate one byte and initialise to 10
Y	    ds.b 1	allocate one byte but don’t initialise
