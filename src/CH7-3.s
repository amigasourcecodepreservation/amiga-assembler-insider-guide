*	Example CH7-3.S

START move.w  X,Y copy word X to word Y 
      rts

X	    dc.w 10	allocate word and initialise to 10
Y	    ds.w 1	allocate word but don’t initialise
